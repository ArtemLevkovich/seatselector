//
//  SeatSelectorViewController.swift
//  SeatSelector
//
//  Created by Artem Levkovich on 9/17/20.
//  Copyright © 2020 Artem Levkovich. All rights reserved.
//

import Foundation
import UIKit

class SeatSelectorViewController: UIViewController, ZSeatSelectorDelegate {

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        view.backgroundColor = .white

        let map2:String =
        "______________________________/" +
        "______________________________/" +
        "______________________________/" +
        "______________________________/" +
        "A_____________________________/" +
        "______________________________/" +
        "A____AAAAAA___AAAAAA___AAAA___/" +
        "______________________________/" +
        "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/" +
        "______________________________/" +
        "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/" +
        "______________________________/"

        let seats2 = ZSeatSelector()
        seats2.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        seats2.setSeatSize(CGSize(width: 30, height: 30))
        seats2.setAvailableImage(   UIImage(named: "A")!,
            andUnavailableImage:    UIImage(named: "U")!,
            andDisabledImage:       UIImage(named: "D")!,
            andSelectedImage:       UIImage(named: "S")!)
        seats2.layout_type = "Normal"
        seats2.setMap(map2)
        seats2.seat_price           = 5.0
        seats2.selected_seat_limit  = 5
        seats2.seatSelectorDelegate = self
        seats2.maximumZoomScale         = 5.0
        seats2.minimumZoomScale         = 0.05
        self.view.addSubview(seats2)

        seats2.translatesAutoresizingMaskIntoConstraints = false
        seats2.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        seats2.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        seats2.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        seats2.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true

        if let image = UIImage(named: "map.png") {
            let img = UIImageView(frame: seats2.frame)
            img.alpha = 0.1
            img.image = image
            seats2.insertSubview(img, at: 0)
        }
    }

    func seatSelected(_ seat: ZSeat) {
        print("Seat at row: \(seat.row) and column: \(seat.column)\n")
    }

    func getSelectedSeats(_ seats: NSMutableArray) {
        var total:Float = 0.0;
        for i in 0..<seats.count {
            let seat:ZSeat  = seats.object(at: i) as! ZSeat
            print("Seat at row: \(seat.row) and column: \(seat.column)\n")
            total += seat.price
        }
        print("----- Total -----\n")
        print("----- \(total) -----\n")
    }
}
