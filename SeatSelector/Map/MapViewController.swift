//
//  MapViewController.swift
//  SeatSelector
//
//  Created by Artem Levkovich on 9/16/20.
//  Copyright © 2020 Artem Levkovich. All rights reserved.
//

import UIKit
import Foundation

class MapViewController: UIViewController {

    var imageScrollView = ImageScrollView()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        title = "Schema"

        view.backgroundColor = .white

        view.addSubview(imageScrollView)
        imageScrollView.translatesAutoresizingMaskIntoConstraints = false
        imageScrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        imageScrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        imageScrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        imageScrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true

        imageScrollView.setup()

        if let mapImg = UIImage(named: "map") {
            imageScrollView.display(image: mapImg)
        }

        imageScrollView.imageScrollViewDelegate = self
    }
}

extension MapViewController: ImageScrollViewDelegate {
    func imageScrollViewDidChangeOrientation(imageScrollView: ImageScrollView) {
        print("Did change orientation")
    }

    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        print("scrollViewDidEndZooming at scale \(scale)")
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scrollViewDidScroll at offset \(scrollView.contentOffset)")
    }
}
