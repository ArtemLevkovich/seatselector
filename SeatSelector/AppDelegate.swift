//
//  AppDelegate.swift
//  SeatSelector
//
//  Created by Artem Levkovich on 9/16/20.
//  Copyright © 2020 Artem Levkovich. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        return true
    }

    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {

        // Make sure the root controller has been set
        // (won't initially be set when the app is launched)
        if let navigationController = window?.rootViewController as? UINavigationController {

            // If the visible view controller is the
            // view controller you'd like to rotate, allow
            // that window to support all orientations
            if navigationController.visibleViewController is MapViewController {
                return UIInterfaceOrientationMask.landscapeLeft
            } else if navigationController.visibleViewController is SeatSelectorViewController {
                return UIInterfaceOrientationMask.landscapeLeft
            }

            // Else only allow the window to support portrait orientation
            else {
                return UIInterfaceOrientationMask.portrait
            }
        }

        return UIInterfaceOrientationMask.portrait
    }
}
