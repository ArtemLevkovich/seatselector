//
//  SeatsViewController+CollectionView.swift
//  SeatSelector
//
//  Created by Artem Levkovich on 9/19/20.
//  Copyright © 2020 Artem Levkovich. All rights reserved.
//

import UIKit
import Foundation
import FirebaseAuth

class SeatCell: UICollectionViewCell {
    
    var textLabel: UILabel = {
        let label: UILabel = UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 16)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
     }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        backgroundColor = .white
        addSubview(textLabel)

        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        textLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        textLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        textLabel.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class SectionHeader: UICollectionReusableView {

    var label: UILabel = {
        let label: UILabel = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        backgroundColor = UIColor(red: 228.0/255.0, green: 228.0/255.0, blue: 228.0/255.0, alpha: 1.0)

        addSubview(label)

        label.translatesAutoresizingMaskIntoConstraints = false
        label.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        label.leftAnchor.constraint(equalTo: leftAnchor, constant: 15).isActive = true
        label.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        label.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SeatsViewController {
    
    /// Setup Collection View
    func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        let size = (view.frame.size.width-10*4-1)/3
        layout.itemSize = CGSize(width: size, height: size)
        layout.headerReferenceSize = CGSize(width: view.frame.size.width, height: 60)
        layout.sectionHeadersPinToVisibleBounds = true

        collectionView = UICollectionView(frame: view.frame, collectionViewLayout: layout)
        collectionView?.register(SeatCell.self, forCellWithReuseIdentifier: "cell")
        collectionView?.register(SectionHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "header")
        collectionView?.backgroundColor = UIColor.white

        collectionView?.dataSource = self
        collectionView?.delegate = self

        view.addSubview(collectionView ?? UICollectionView())

        collectionView?.reloadData()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return seats.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SeatCell

        let seat = seats[indexPath.row]
        let booked = isBooked(seat: seat)
        let bookedByCurrentUser = isBookedByCurrentUser(seat: seat)
        let seatNumber = getSeatID(seat: seat)

        cell.backgroundColor = booked ? UIColor(red: 250.0/255.0, green: 219.0/255.0, blue: 93.0/255.0, alpha: 1.0) : UIColor(red: 130.0/255.0, green: 235.0/255.0, blue: 125.0/255.0, alpha: 1.0)
        if bookedByCurrentUser {
            cell.backgroundColor = UIColor(red: 69.0/255.0, green: 95.0/255.0, blue: 228.0/255.0, alpha: 1.0)
        }

        if booked, let email = getUserInfo(seat: seat) {
            let owner = email.replacingOccurrences(of: "@itrexgroup.com", with: "")
            cell.textLabel.text = bookedByCurrentUser ? "#\(seatNumber)\n\nbooked by\nyourself" : "#\(seatNumber)\n\nbooked by\n\(owner)"
            cell.textLabel.font = UIFont.boldSystemFont(ofSize: 15)
        } else {
            cell.textLabel.text = "#\(seatNumber)"
            cell.textLabel.font = UIFont.boldSystemFont(ofSize: 25)
        }

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let seat = seats[indexPath.row]
        selectSeat(seat: seat)
    }

    /// Header
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 60)
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
             let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as! SectionHeader
            var title = currentTimeStamp
            if let date = currentTimeStamp, let user = Auth.auth().currentUser, let email = user.email {
                let owner = email.replacingOccurrences(of: "@itrexgroup.com", with: "")
                title = "User: \(owner)\nDate: \(date)"
            }
            sectionHeader.label.text = title
            return sectionHeader
        } else {
             return UICollectionReusableView()
        }
    }
}
