//
//  SeatsViewController+Data.swift
//  SeatSelector
//
//  Created by Artem Levkovich on 9/19/20.
//  Copyright © 2020 Artem Levkovich. All rights reserved.
//

import UIKit
import Foundation
import Firebase

/// Data processing
extension SeatsViewController {

    func setupDB() {
        /// Setup DB
        ref = Database.database().reference()

        ref.child("places").observeSingleEvent(of: .value, with: { [weak self] (snapshot) in
            guard let self = self, let data = snapshot.value else { return }
            self.processSeats(data: data)
        }) { (error) in
            print(error.localizedDescription)
        }

        ref.child("places").observe(.value) { [weak self] snapshot in
            guard let self = self, let data = snapshot.value else { return }
            self.processSeats(data: data)
        }

        ref.child("bookings").observeSingleEvent(of: .value, with: { [weak self] (snapshot) in
            guard let self = self, let data = snapshot.value else { return }
            self.processBookings(data: data)
        }) { (error) in
            print(error.localizedDescription)
        }

        ref.child("bookings").observe(.value) { [weak self] snapshot in
            guard let self = self, let data = snapshot.value else { return }
            self.processBookings(data: data)
        }
    }

    func processSeats(data: Any) {
        seats = []

        if let seatsList = data as? [AnyObject] {
            for seatObj in seatsList {
                let seat = Seat(fromDictionary: seatObj)
                seats.append(seat)
            }
        }

        tableView.reloadData()
        collectionView?.reloadData()
    }

    func processBookings(data: Any) {
        bookings = []

        if let bookingsList = data as? [String: Any] {
            for bookingObj in bookingsList {
                if let bookingData = bookingObj.value as? [String: Any] {
                    let booking = Booking(fromDictionary: bookingData)
                    booking.bookingID = bookingObj.key
                    bookings.append(booking)
                }
            }
        }

        bookingVC.bookingsList = bookings

        tableView.reloadData()
        collectionView?.reloadData()
    }

    func getSeatID(seat: Seat) -> String {
        if let seatID = seat.seatID {
            return seatID
        }
        return "0"
    }

    func isCurrentUser(booking: Booking) -> Bool {
        if let email = booking.email, let user = Auth.auth().currentUser, let userEmail = user.email, email == userEmail {
            return true
        }
        return false
    }

    func isCurrentDate(booking: Booking) -> Bool {
        if let bookingDate = booking.date, let currantDate = currentTimeStamp, bookingDate == currantDate {
            return true
        }
        return false
    }

    func isBooked(seat: Seat) -> Bool {
        var isBooked = false
        for booking in bookings {
            if isCurrentDate(booking: booking), getSeatID(seat: seat) == booking.placeNumber {
                isBooked = true
                break
            }
        }
        return isBooked
    }

    func isBookedByCurrentUser(seat: Seat) -> Bool {
        var isBooked = false
        for booking in bookings {
            if isCurrentDate(booking: booking), getSeatID(seat: seat) == booking.placeNumber, isCurrentUser(booking: booking) {
                isBooked = true
                break
            }
        }
        return isBooked
    }

    func getBookingID(seat: Seat) -> String? {
        for booking in bookings {
            if isCurrentDate(booking: booking), isCurrentUser(booking: booking), getSeatID(seat: seat) == booking.placeNumber {
                return booking.bookingID
            }
        }
        return nil
    }

    func getUserInfo(seat: Seat) -> String? {
        for booking in bookings {
            if isCurrentDate(booking: booking), getSeatID(seat: seat) == booking.placeNumber, let email = booking.email {
                return email
            }
        }
        return nil
    }

    func cancelOtherBookings(date: String) {
        for booking in bookings {
            if let bookingDate = booking.date, bookingDate == date, isCurrentUser(booking: booking) {
                /// Remove other bookings
                if let bookingID = booking.bookingID {
                    let itemRef = ref.child("bookings/\(bookingID)")
                    itemRef.removeValue()
                }
            }
        }
    }

    func getBookedDays(seat: Seat) -> [Date] {
        var days: [Date] = []
        let sortedBookings = bookings.sorted {
            $0.dateObject < $1.dateObject
        }

        let bookingsCount = sortedBookings.count
        if bookingsCount > 0 {
            for i in 0...bookingsCount-1 {
                let booking = sortedBookings[i]
                if let _ = booking.date, let _ = booking.email, booking.placeNumber == getSeatID(seat: seat) {
                    days.append(booking.dateObject)
                }
            }
        }
        return days
    }

    func getBookedDaysByCurrentUser(seat: Seat) -> [Date] {
        var days: [Date] = []
        let sortedBookings = bookings.sorted {
            $0.dateObject < $1.dateObject
        }

        let bookingsCount = sortedBookings.count
        if bookingsCount > 0 {
            for i in 0...bookingsCount-1 {
                let booking = sortedBookings[i]
                if let _ = booking.date, isCurrentUser(booking: booking) {
                    days.append(booking.dateObject)
                }
            }
        }
        return days
    }
}
