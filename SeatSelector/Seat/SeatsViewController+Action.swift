//
//  SeatsViewController+Action.swift
//  SeatSelector
//
//  Created by Artem Levkovich on 9/19/20.
//  Copyright © 2020 Artem Levkovich. All rights reserved.
//

import UIKit
import Foundation
import FirebaseAuth

/// Actions
extension SeatsViewController {

    @objc func logoutTapped() {
        print("logoutTapped")

        do {
            try Auth.auth().signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        
        navigationController?.popViewController(animated: true)
    }

    @objc func showBookedListTapped() {
        print("showBookedListTapped")

        bookingVC.bookingsList = bookings
        bookingVC.cancelBookingClosure = { [weak self] command in
            let itemRef = self?.ref.child(command)
            itemRef?.removeValue()
        }
        navigationController?.pushViewController(bookingVC, animated: true)
    }

    @objc func mapTapped() {
        print("mapTapped")

        let vc = MapViewController()
        //let vc = SeatSelectorViewController()
        navigationController?.pushViewController(vc, animated: true)
    }

    @objc func updateGridTapped() {
        print("updateGridTapped")

        listMode.toggle()
        if listMode {
            tableView.alpha = 1.0
            collectionView?.alpha = 0.0
        } else {
            tableView.alpha = 0.0
            collectionView?.alpha = 1.0
        }
    }
}
