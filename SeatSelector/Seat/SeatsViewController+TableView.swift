//
//  SeatsViewController+TableView.swift
//  SeatSelector
//
//  Created by Artem Levkovich on 9/19/20.
//  Copyright © 2020 Artem Levkovich. All rights reserved.
//

import UIKit
import Foundation
import FirebaseAuth

class TableHeader: UIView {

    var label: UILabel = {
        let label: UILabel = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        backgroundColor = UIColor(red: 228.0/255.0, green: 228.0/255.0, blue: 228.0/255.0, alpha: 1.0)

        addSubview(label)

        label.translatesAutoresizingMaskIntoConstraints = false
        label.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        label.leftAnchor.constraint(equalTo: leftAnchor, constant: 15).isActive = true
        label.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        label.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class TableFooter: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)

        heightAnchor.constraint(equalToConstant: 1).isActive = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SeatsViewController {
    
    /// Setup Table View
    func setupTableView() {
        view.addSubview(tableView)

        tableView.backgroundColor = .clear
        tableView.delegate = self
        tableView.dataSource = self

        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")

        tableView.reloadData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return seats.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        let seat = seats[indexPath.row]
        let booked = isBooked(seat: seat)
        let bookedByCurrentUser = isBookedByCurrentUser(seat: seat)
        let seatNumber = getSeatID(seat: seat)

        if booked, let email = getUserInfo(seat: seat) {
            let owner = email.replacingOccurrences(of: "@itrexgroup.com", with: "")
            cell.textLabel?.text = bookedByCurrentUser ? "Place #\(seatNumber) (booked by yourself)" : "Place #\(seatNumber) (booked by \(owner))"
        } else {
            cell.textLabel?.text = "Place #\(seatNumber)"
        }

        cell.imageView?.image = booked ? UIImage(named: "U") : UIImage(named: "A")
        if bookedByCurrentUser {
            cell.imageView?.image = UIImage(named: "D")
        }

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)

        let seat = seats[indexPath.row]
        selectSeat(seat: seat)
    }

    /// Header

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        60
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = TableHeader()
        if let date = currentTimeStamp, let user = Auth.auth().currentUser, let email = user.email {
            let owner = email.replacingOccurrences(of: "@itrexgroup.com", with: "")
            header.label.text = "User: \(owner)\nDate: \(date)"
        } else {
            header.label.text = currentTimeStamp
        }
        return header
    }

    /// Footer

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        1
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = TableFooter()
        return footer
    }
}
