//
//  SeatModels.swift
//  SeatSelector
//
//  Created by Artem Levkovich on 9/19/20.
//  Copyright © 2020 Artem Levkovich. All rights reserved.
//

import Foundation

/// Seat model
class Seat: NSObject {

    var seatID: String? = ""

    init(fromDictionary dictionary: AnyObject) {
        seatID = dictionary["id"] as? String
    }
}

/// Booking model
class Booking: NSObject {

    var bookingID: String? = ""

    var email: String?
    var placeNumber: String?

    var date: String?
    var dateObject = Date()

    init(fromDictionary dictionary: [String:Any]) {

        placeNumber = dictionary["place"] as? String
        email = dictionary["email"] as? String
        date = dictionary["date"] as? String

        let df = DateFormatter()
        df.dateFormat = "dd.MM.yyyy"
        if let dateStr = date {
            dateObject = df.date(from: dateStr) ?? Date()
        }
    }
}
