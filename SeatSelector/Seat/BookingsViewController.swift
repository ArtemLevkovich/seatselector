//
//  BookingsViewController.swift
//  SeatSelector
//
//  Created by Artem Levkovich on 9/19/20.
//  Copyright © 2020 Artem Levkovich. All rights reserved.
//

import UIKit
import Foundation
import FirebaseAuth

class BookingsTableFooter: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)

        heightAnchor.constraint(equalToConstant: 1).isActive = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class BookingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let tableView = UITableView()

    var bookingsList: [Booking] = [] {
        didSet {
            /// Filter Bookings
            filterBookings()
        }
    }
    var sortedBookings: [Booking] = []

    public var cancelBookingClosure:((String) -> ())? = nil

    public var placeNumberOrder = true

    /// Life circle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        view.backgroundColor = .white

        title = "My Bookings"

        /// Setup navigation bar
        let sort = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(changeSorting))
        navigationItem.rightBarButtonItems = [sort]

        /// Setup Table view
        view.addSubview(tableView)

        tableView.backgroundColor = .clear
        tableView.delegate = self
        tableView.dataSource = self

        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")

        /// Filter Bookings
        filterBookings()

        /// Update Bookings order
        updateOrder()
    }

    func filterBookings() {
        var bookings: [Booking] = []

        let seatBookings = bookingsList

        let sorted = seatBookings.sorted {
            $0.dateObject < $1.dateObject
        }

        for booking in sorted {
            if let email = booking.email, let user = Auth.auth().currentUser, let userEmail = user.email, email == userEmail {
                /// Append
                bookings.append(booking)
            }
        }

        sortedBookings = bookings

        /// Update Bookings order
        updateOrder()
    }

    @objc func changeSorting() {
        print("changeSorting")

        placeNumberOrder.toggle()

        /// Update Bookings order
        updateOrder()
    }

    func updateOrder() {

        if placeNumberOrder {
            sortedBookings = sortedBookings.sorted {
                Int($0.placeNumber ?? "0") ?? 0 < Int($1.placeNumber ?? "0") ?? 0
            }
        } else {
            sortedBookings = sortedBookings.sorted {
                $0.dateObject < $1.dateObject
            }
        }

        tableView.reloadData()
    }
}

extension BookingsViewController {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortedBookings.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        let booking = sortedBookings[indexPath.row]

        if let date = booking.date, let placeNumber = booking.placeNumber {
            cell.textLabel?.text = "Place #\(placeNumber): \(date)"

            let normalAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15.0)]
            let boldAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15.0)]

            let beginString = NSMutableAttributedString(string: "Place ", attributes: normalAttributes)
            let placeNumberString = NSMutableAttributedString(string: "#\(placeNumber)", attributes: placeNumberOrder ? boldAttributes : normalAttributes)
            let separatorString = NSAttributedString(string: ": ", attributes: normalAttributes)
            let dateString = NSAttributedString(string: "\(date)", attributes: placeNumberOrder ? normalAttributes : boldAttributes)

            beginString.append(placeNumberString)
            beginString.append(separatorString)
            beginString.append(dateString)

            cell.textLabel?.attributedText = beginString
        }

        cell.imageView?.image = UIImage(named: "D")

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)

        let booking = sortedBookings[indexPath.row]

        var title = "Place #"
        if let placeNumber = booking.placeNumber {
            title = "Place #\(placeNumber)"
        }
        var dateTitle = ""
        if let date = booking.date {
            dateTitle = date
        }

        /// Process case when seat was booking by current user - so we could cancel it
        let alert = UIAlertController(title: title, message: dateTitle, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Cancel booking", style: .default , handler:{ [weak self] (UIAlertAction)in
            print("Cancel booking")

            if let bookingID = booking.bookingID {
                let command = "bookings/\(bookingID)"
                if let closure = self?.cancelBookingClosure {
                    closure(command)
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("Dismiss")
        }))
        present(alert, animated: true, completion: {
        })
    }

    /// Footer

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        1
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = TableFooter()
        return footer
    }
}
