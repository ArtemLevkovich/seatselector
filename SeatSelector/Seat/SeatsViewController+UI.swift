//
//  SeatsViewController+UI.swift
//  SeatSelector
//
//  Created by Artem Levkovich on 9/19/20.
//  Copyright © 2020 Artem Levkovich. All rights reserved.
//

import UIKit
import Foundation

/// UI
extension SeatsViewController {
    /// Setup Navigation Buttons
    func showCommonNavigationButtons() {
        title = "Places"

        //let map = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(mapTapped))
        let calendar = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(calendarFilterTapped))
        let grid = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(updateGridTapped))
        navigationItem.rightBarButtonItems = [/*map, */calendar, grid]
        let logout = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(logoutTapped))
        let bookings = UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: #selector(showBookedListTapped))
        navigationItem.leftBarButtonItems = [logout, bookings]
        navigationItem.hidesBackButton = true
    }
}
