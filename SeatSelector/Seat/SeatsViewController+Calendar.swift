//
//  SeatsViewController+Calendar.swift
//  SeatSelector
//
//  Created by Artem Levkovich on 9/19/20.
//  Copyright © 2020 Artem Levkovich. All rights reserved.
//

import UIKit
import Foundation
import Firebase
//import WWCalendarTimeSelector

/// Calendar
extension SeatsViewController: WWCalendarTimeSelectorProtocol {

    @objc func calendarFilterTapped() {
        print("calendarFilterTapped")

        calendar = WWCalendarTimeSelector.instantiate()
        calendar.delegate = self

        calendar.optionCurrentDate = singleDate
        calendar.optionCurrentDates = Set(multipleDates)
        calendar.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
        calendar.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)

        calendar.optionStyles.showDateMonth(true)
        calendar.optionStyles.showMonth(false)
        calendar.optionStyles.showYear(false)
        calendar.optionStyles.showTime(false)

        calendar.shouldDisableSelection = false
        
        present(calendar, animated: true, completion: nil)
    }

    @objc func calendarMultiselectTapped(days: [Date], currentUserDates: [Date]? = []) {
        print("calendarMultiselectTapped")

        calendar = WWCalendarTimeSelector.instantiate()
        calendar.delegate = self

        calendar.optionCurrentDate = singleDate

        calendar.blockedDates = Set(days)
        if let dates = currentUserDates {
            calendar.currentUserBlockedDates = Set(dates)
        }
        calendar.optionCurrentDates = Set()

        calendar.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
        calendar.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)

        calendar.optionSelectionType = WWCalendarTimeSelectorSelection.multiple
        calendar.optionMultipleSelectionGrouping = .simple

        calendar.shouldDisableSelection = false

        present(calendar, animated: true, completion: nil)
    }

    /// WWCalendar Delegates
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print("Selected \n\(date)\n---")
        singleDate = date

        currentTimeStamp = dateFormatter.string(from: date)

        tableView.reloadData()
        collectionView?.reloadData()
    }

    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, dates: [Date]) {
        print("Selected Multiple Dates \n\(dates)\n---")

        multipleDates = dates

        for date in dates {
            let timestamp = dateFormatter.string(from: date)

            /// Cancel other bookings
            cancelOtherBookings(date: timestamp)
        }

        for date in dates {
            let timestamp = dateFormatter.string(from: date)

            //let newBookingKey = UUID().uuidString
            let itemRef = ref.child("bookings").childByAutoId()
            let messageItem = [
                "email": selectedSeatEmail,
                "date": timestamp,
                "place": selectedSeatID,
                "userId": Auth.auth().currentUser?.uid ?? ""
                ] as [String : Any]
            itemRef.setValue(messageItem)
        }
    }
}
