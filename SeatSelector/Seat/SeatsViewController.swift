//
//  SeatsViewController.swift
//  SeatSelector
//
//  Created by Artem Levkovich on 9/16/20.
//  Copyright © 2020 Artem Levkovich. All rights reserved.
//

import UIKit
import Foundation
import Firebase
import FirebaseAuth

/// Class dislays Seat list
class SeatsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource {

    /// Properties

    var ref: DatabaseReference!
    var seats: [Seat] = []
    var bookings: [Booking] = []

    let tableView = UITableView()
    var collectionView: UICollectionView? = nil

    var currentTimeStamp: String? = nil
    let dateFormatter: DateFormatter = DateFormatter()

    var listMode = true

    var singleDate: Date = Date()
    var multipleDates: [Date] = []

    var selectedSeatID = ""
    var selectedSeatEmail = ""

    var bookingVC = BookingsViewController()

    var calendar: WWCalendarTimeSelector = WWCalendarTimeSelector.instantiate()

    /// Life circle

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // Hide the navigation bar on the this view controller
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white

        /// Init data
        dateFormatter.dateFormat = "dd.MM.yyyy"
        currentTimeStamp = dateFormatter.string(from: NSDate() as Date)

        /// Setup UI
        showCommonNavigationButtons()
        setupTableView()
        setupCollectionView()
        collectionView?.alpha = 0.0

        /// Setup DB
        setupDB()
    }
}

extension SeatsViewController {

    func selectSeat(seat: Seat) {
        if let user = Auth.auth().currentUser, let email = user.email {

            print("EMAIL = ", email)

            /// Save selected data
            selectedSeatID = seat.seatID ?? ""
            selectedSeatEmail = email

            let booked = isBooked(seat: seat)
            let bookedByCurrentUser = isBookedByCurrentUser(seat: seat)

            let title = "Place #\(getSeatID(seat: seat))"

            /// Process case when seat wasn't booked yet
            if !booked {
                /*
                let daysByCurrentUser = getBookedDaysByCurrentUser(seat: seat)
                let days = getBookedDays(seat: seat)
                calendarMultiselectTapped(days: days, currentUserDates: daysByCurrentUser)
                */

                let timestamp = currentTimeStamp ?? ""

                /// Cancel other bookings
                cancelOtherBookings(date: timestamp)

                /// Book
                let itemRef = ref.child("bookings").childByAutoId()
                let messageItem = [
                    "email": selectedSeatEmail,
                    "date": timestamp,
                    "place": selectedSeatID,
                    "userId": Auth.auth().currentUser?.uid ?? ""
                    ] as [String : Any]
                itemRef.setValue(messageItem)

            } else {
                /// Process case when seat was booking by someone else
                if !bookedByCurrentUser {

                    /*
                    let daysByCurrentUser = getBookedDaysByCurrentUser(seat: seat)
                    let days = getBookedDays(seat: seat)
                    calendarMultiselectTapped(days: days, currentUserDates: daysByCurrentUser)
                    */
                } else {
                    /// Process case when seat was booking by current user - so we could cancel it
                    let alert = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
                    alert.addAction(UIAlertAction(title: "Cancel booking", style: .default , handler:{ [weak self] (UIAlertAction)in
                        print("Cancel booking")

                        if let bookingID = self?.getBookingID(seat: seat) {
                            let itemRef = self?.ref.child("bookings/\(bookingID)")
                            itemRef?.removeValue()
                        }
                    }))
                    /*
                    alert.addAction(UIAlertAction(title: "Book other days", style: .default , handler:{ [weak self] (UIAlertAction)in
                        print("Book other days")

                        let daysByCurrentUser = self?.getBookedDaysByCurrentUser(seat: seat)
                        if let days = self?.getBookedDays(seat: seat) {
                            self?.calendarMultiselectTapped(days: days, currentUserDates: daysByCurrentUser)
                        }
                    }))
                    */
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                        print("Dismiss")
                    }))
                    present(alert, animated: true, completion: {
                    })
                }
            }
        } else {
            print("Empty user. Please, sign in!")
        }
    }
}
