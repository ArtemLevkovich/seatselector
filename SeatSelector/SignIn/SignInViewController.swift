//
//  SignInViewController.swift
//  SeatSelector
//
//  Created by Artem Levkovich on 9/19/20.
//  Copyright © 2020 Artem Levkovich. All rights reserved.
//

import UIKit
import FirebaseAuth

class SignInViewController: UIViewController, UITextFieldDelegate {

    var emailTextField = UITextField()
    var passwordTextField = UITextField()

    let spinner = UIActivityIndicatorView(style: .whiteLarge)

    /// Life circle

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // Hide the navigation bar on the this view controller
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        view.backgroundColor = .black

        title = "Account"

        /// Add Email text field
        emailTextField =  UITextField(frame: CGRect(x: 30, y: 140, width: view.frame.width-60, height: 40))
        emailTextField.placeholder = "Email (excluding @itrexgroup.com)"
        emailTextField.font = UIFont.systemFont(ofSize: 15)
        emailTextField.borderStyle = .roundedRect
        emailTextField.autocorrectionType = .no
        emailTextField.keyboardType = .emailAddress
        emailTextField.textContentType = .username
        emailTextField.returnKeyType = .done
        emailTextField.clearButtonMode = .whileEditing
        emailTextField.contentVerticalAlignment = .center
        emailTextField.autocapitalizationType = .none
        emailTextField.delegate = self
        view.addSubview(emailTextField)

        /// Add Password text field
        passwordTextField =  UITextField(frame: CGRect(x: 30, y: 200, width: view.frame.width-60, height: 40))
        passwordTextField.placeholder = "Password"
        passwordTextField.font = UIFont.systemFont(ofSize: 15)
        passwordTextField.borderStyle = .roundedRect
        passwordTextField.autocorrectionType = .no
        passwordTextField.keyboardType = .default
        passwordTextField.textContentType = .password
        passwordTextField.returnKeyType = .done
        passwordTextField.clearButtonMode = .whileEditing
        passwordTextField.contentVerticalAlignment = .center
        passwordTextField.isSecureTextEntry = true
        passwordTextField.autocapitalizationType = .none
        passwordTextField.delegate = self
        view.addSubview(passwordTextField)

        /// Add Sign In button
        let button = UIButton()
        button.frame = CGRect(x: 50, y: 280, width: view.frame.width-100, height: 50)
        button.setTitle("Sign In", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15.0)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action:#selector(signInAction), for: .touchUpInside)
        button.backgroundColor = .white
        button.layer.cornerRadius = 8.0
        button.layer.masksToBounds = true
        view.addSubview(button)

        /// Add logo
        let imageView = UIImageView()
        imageView.frame = CGRect(x: (view.frame.width-150)/2, y: 400, width: 150, height: 150)
        if let logo = UIImage(named: "logo") {
            imageView.image = logo
        }
        view.addSubview(imageView)

        /// Add spinner
        spinner.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(spinner)
        spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true

        /// Check Auth
        if Auth.auth().currentUser != nil {
            print("User is signed in.")

            let vc = SeatsViewController()
            navigationController?.pushViewController(vc, animated: true)
        } else {
            print("No user isn't signed in.")

            signInCall()
        }

        /// Add tap recognizer to close the keyboard
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        view.addGestureRecognizer(tap)
    }

    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }

    @objc func signInAction() {
        print("signInAction")

        signInCall()
    }

    func signInCall() {

        let emailValue = emailTextField.text
        let passwordValue = passwordTextField.text

        if let email = emailValue, let password = passwordValue, email != "", password != "" {

            let tunedEmail = email.trimmingCharacters(in: .whitespacesAndNewlines) + "@itrexgroup.com"

            spinner.startAnimating()

            //Auth.auth().createUser(withEmail: tunedEmail, password: password)
            Auth.auth().signIn(withEmail: tunedEmail, password: password) { [weak self] authResult, error in
                guard let self = self else {
                    return
                }

                self.spinner.stopAnimating()

                if let userEmail = authResult?.user.email {
                    print("EMAIL = ", userEmail)

                    print("NC = ", self.navigationController)

                    let vc = SeatsViewController()
                    self.navigationController?.pushViewController(vc, animated: false)
                } else if let localizedDescription = error?.localizedDescription {
                    print("ERROR = ", localizedDescription)

                    let infoAlert = UIAlertController(title: "Error:", message: localizedDescription, preferredStyle: .alert)
                    self.present(infoAlert, animated: true, completion: {
                    })
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                        infoAlert.dismiss(animated: true, completion: nil)
                    }
                }
            }
        } else {
            signOut()
        }
    }

    func signOut() {
        do {
            try Auth.auth().signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
}

/// TextField delegates
extension SignInViewController {

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        // return NO to disallow editing.
        print("TextField should begin editing method called")
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        // became first responder
        print("TextField did begin editing method called")
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
        print("TextField should snd editing method called")
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        // may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
        print("TextField did end editing method called")
    }

    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        // if implemented, called in place of textFieldDidEndEditing:
        print("TextField did end editing with reason method called")
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // return NO to not change text
        print("While entering the characters this method gets called")
        return true
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        // called when clear button pressed. return NO to ignore (no notifications)
        print("TextField should clear method called")
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // called when 'return' key pressed. return NO to ignore.
        print("TextField should return method called")
        // may be useful: textField.resignFirstResponder()
        return true
    }
}

